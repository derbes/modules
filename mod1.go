package modules

import "math"

func SquareRoot(a,b,c int) (int, int){
	var temp = float64(b*b - 4*a*c)

	return (-b + int(math.Sqrt(temp)))/(2*a), (-b - int(math.Sqrt(temp)))/(2*a)
}

func Conc(str1,str2 string) (string, string) {
	temp:=str1[0]
	return str1 + str2, string(temp)
}
func ShiftLeft(number int) int{
	return (number<<1);
}
func ShiftRight(number int) int{
	return (number>>1);
}

